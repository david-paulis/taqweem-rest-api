var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt = require('jsonwebtoken');
var User = require('./models/user');

exports.local = passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

const SECRET_KEY = '12345-67890-09876-54321';

exports.getToken = function(user) {
    return jwt.sign(user, SECRET_KEY,
        {expiresIn: 3600});
};

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = SECRET_KEY ;

exports.jwtPassport = passport.use(new JwtStrategy(opts,
    (jwt_payload, done) => {
        console.log("JWT payload: ", jwt_payload);
        User.findOne({_id: jwt_payload._id}, (err, user) => {
            if (err) {
                return done(err, false);
            }
            else if (user) {
                return done(null, user);
            }
            else {
                return done(null, false);
            }
        });
    }));

exports.verifyUser = passport.authenticate('jwt', {session: false});
exports.verifyOwner = (req,res,next)=>{
    if(req.params.userId ==req.user.id){
        next()
    }else {
        res.statusCode = 403;
        res.end("You are not authorized to perform this operation!")
        next(err);
    }
};

exports.verifyAdmin = (req,res,next)=>{
    if(req.user.admin){
        next();
       
    }else {
         
         res.statusCode= 403;
         res.end("You are not authorized to perform this operation!")
         next(err);
    }
};