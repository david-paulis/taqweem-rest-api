var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
var authenticate = require('../authenticate');
var passport = require('passport');
var User =require('../models/user');
router.route('/')
.get(authenticate.verifyUser,authenticate.verifyAdmin,(req,res,next)=>{
    User.find({})
     .then((users)=>{
         res.statusCode =200;
         res.setHeader('Content-Type','application/json');
         res.json(users);
     },(err)=> next(err))
     .catch((err)=> next(err));
})
.post((req,res,next)=>{
    User.register(new User({username:req.body.username ,firstName:req.body.firstName,lastName:req.body.lastName
    ,email:req.body.email,avatar:req.body.avatar}),req.body.password,(err,user)=>{
    if(err){
        res.statusCode = 500;
        res.setHeader('Content-Type', 'application/json');
        res.json({err: err});
    }else {
        passport.authenticate('local')(req, res, () => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json({success: true, status: 'Registration Successful!'});})
    }
});
    
})
.delete(authenticate.verifyUser,authenticate.verifyAdmin,(req,res,next)=>{
    User.deleteMany({})
      .then((response)=>{
        res.statusCode =200;
        res.setHeader('Content-Type','application/json');
        res.json(response);
      }, err => next(err))
      .catch((err)=>next(err))
})
.put(authenticate.verifyUser,authenticate.verifyAdmin,(req,res,next)=>{
    res.statusCode = 403;
    res.end('POST operation not supported on /users');
});
router.post('/login', passport.authenticate('local'),(req,res,next)=>{
    var token = authenticate.getToken({_id: req.user._id});
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json({success: true,token: token ,status: 'You are successfully logged in!'});
});
router.route('/:userId')
    .get(authenticate.verifyUser,authenticate.verifyOwner,(req,res,next)=>{
        User.findById(req.params.userId)
            .then((user)=>{
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(user);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(authenticate.verifyUser,authenticate.verifyOwner,(req,res,next)=>{
        res.statusCode = 403;
        res.end('POST operation not supported on /users/' + req.params.userId);
    })
    .put(authenticate.verifyUser,authenticate.verifyOwner,(req,res,next)=>{
        User.findByIdAndUpdate(req.params.userId,{
            $set: req.body
        },{new: true})
        .then((user)=>{
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(user);
        },err=> next(err))
        .catch((err)=> next(err));
    })
    .delete(authenticate.verifyUser,authenticate.verifyOwner,(req,res,next)=>{
        User.findByIdAndRemove(req.params.userId)
           .then((resp)=>{
               res.statusCode=200;
               res.setHeader('Content-Type', 'application/json');
               res.json(resp);
           },err=>next(err))
           .catch((err) => next(err));

    });

    module.exports = router;