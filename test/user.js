var should = require("should");
var request = require("request");
var expect = require("chai").expect;
var baseUrl = "http://localhost:3000/users/";
var util = require("util");

describe(" /user ", function () {
    it("get /user", function (done) {
        request.get({ url: baseUrl }, function (error, response, body) {
            expect(response.statusCode).to.equal(200);
            console.log(body);
            done();
        })
    });
    /*{"username":"pop","password":"123456","firstName":"dod"
    ,"lastName":"paulis","email":"david_fci@outlook.com"}  */
    it("post /user valid case ", function (done) {
        request.post(baseUrl, {
            form: {
                username: "popk", password: "123456", firstName: "dod"
                , lastName: "paulis", email: "david_fci@outlook.com"
            }
        }, function (error, response, body) {
            expect(response.statusCode).to.equal(200);
            console.log(body);
            done();
        });
    });

    it("post /user not firstName case ", function (done) {
        request.post(baseUrl, {
            form: {
                username: "popi", password: "123456"
                , lastName: "paulis", email: "david_fci@outlook.com"
            }
        }, function (error, response, body) {
            expect(response.statusCode).to.equal(500);
            console.log(body);
            done();
        });
    });

});
it("post users/login", function (done) {
    request.post(baseUrl + "login", { form: { username: "pop", password: "123456" } }, function (error, response, body) {
        expect(response.statusCode).to.equal(200);
        console.log(body);
        done();
    });
});
describe("/user/:userId", function () {
    it("update user", function (done) {
        request.put(baseUrl + "5ba551d24d45bc4384b611b6"
            , {
                'auth':
                    { 'bearer': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YmE1NTFkMjRkNDViYzQzODRiNjExYjYiLCJpYXQiOjE1Mzc1NjI3MjcsImV4cCI6MTUzNzU2NjMyN30.1ENjzoM7eE8BK1bfYRnoMXWljIZxqjJ9hvv9HXWo5wQ" },
                 form:{firstName:"dosh"}   
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(200);
                console.log(body);
                done();
            });
    });
    it("delete user false case ", function (done) {
        request.delete(baseUrl + "5ba5561d4d45bc4384b"
            , {
                'auth':
                    { 'bearer': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YmE1NTYxZDRkNDViYzQzODRiNjExYmMiLCJpYXQiOjE1Mzc1NjIxNTksImV4cCI6MTUzNzU2NTc1OX0.93iSG4c8t6mgiw1B5fexKUgmKGaiP4Te8eus4WBAwJQ" }
            }
            , function (error, response, body) {
                expect(response.statusCode).not.equal(200);
                console.log(body);
                done();
            });
    });

});


