const mongoose = require('mongoose');
const Error = mongoose.Error;
var createError = require('http-errors');
const express = require('express');
var logger =require('morgan');
mongoose.Promise = require('bluebird');
const usersRouter= require('./routes/usersRouter');
const uploadRouter= require('./routes/uploadRouter');
const bodyParser = require('body-parser');
var path = require('path');

var app= express();

var passport = require('passport');
var authenticate = require('./authenticate');
app.use(passport.initialize());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/users',usersRouter);
app.use('/imageUpload',uploadRouter);

// catch 404 and then pass error 
app.use(function (req, res, next) {
    next(createError(404));
  });
  
  app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
  
  
  module.exports = app;
  
//const url ='mongodb://localhost:27017/taqweem'; ---------> use this uri if you run on localhost not in container.

const url ='mongodb://mongo:27017/taqweem';
const connect = mongoose.connect(url);
connect.then((db)=>{
    console.log("connected correctly to server");
},(err)=>{ console.log(err); });