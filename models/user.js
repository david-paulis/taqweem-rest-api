const mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');
const User = mongoose.Schema;
require('mongoose-type-email');

const userSchema = new User({
    email: {
        type: mongoose.SchemaTypes.Email
    },

    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    avatar: {
        type: String
    },
    admin:{
        type:Boolean,
        default:false
    }
},
    {
        timestamps: true
    }
);
userSchema.plugin(passportLocalMongoose);
module.exports= mongoose.model('User',userSchema);

